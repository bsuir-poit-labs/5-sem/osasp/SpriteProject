#pragma once

#include <windows.h>;

class Sprite
{
private:
	int left = 0;
	int top = 0;
	int height = 0;
	int width = 0;
	int speed = 10;
	BOOL type = FALSE;
	HWND hwnd;
	HBRUSH hBrush;

	int windowHeight = 0;
	int windowWidth = 0;

	void DrawRect(HDC);
	void DrawImg(HDC, HANDLE);
	void PushOff(int, int);

public:
	Sprite();
	Sprite(HWND);
	void Draw(HDC, HANDLE);
	void Move(int, int);
	void ChanSpriteType();
	void SetSpriteType(BOOL spriteType);
	void SetWindowSize(int heigth, int width);
};




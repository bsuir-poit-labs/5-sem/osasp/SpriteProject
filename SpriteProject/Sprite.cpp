#include "Sprite.h"

Sprite::Sprite()
{
	hBrush = CreateSolidBrush(RGB(255, 0, 0));
	this->hwnd = NULL;
}

Sprite::Sprite(HWND hwnd)
{
	hBrush = CreateSolidBrush(RGB(255, 0, 0));
	this->hwnd = hwnd;

	left = 500;
	top = 200;

	height = 128;
	width = 128;
}

void Sprite::Draw(HDC hdc, HANDLE hBitmap)
{
	type ? DrawImg(hdc, hBitmap) : DrawRect(hdc);
}

void Sprite::DrawRect(HDC hdc)
{
	SelectObject(hdc, hBrush);
	Rectangle(hdc, left, top, left + width, top + height);
	ValidateRect(hwnd, NULL);
}

void Sprite::DrawImg(HDC hdc, HANDLE hBitmap)
{
	HDC hmdc = CreateCompatibleDC(hdc);
	SelectObject(hdc, hBitmap);
	BitBlt(hdc, left, top, left + width, top + height, hmdc, 0, 0, SRCAND);
}

void Sprite::Move(int x, int y)
{
	left += x * speed;
	top += y * speed;
	InvalidateRect(hwnd, NULL, true);

	if ((left < 0) || (top < 0)
		|| ((left + width) > windowWidth)
		|| ((top + height) > windowHeight))
	{
		this->PushOff(-x, -y);
	}
}

void Sprite::ChanSpriteType()
{
	type = !type;
	InvalidateRect(hwnd, NULL, true);
}

void Sprite::PushOff(int x, int y)
{
	for (int i = 0; i < 10; i++)
	{
		this->Move(x, y);
		UpdateWindow(hwnd);
		Sleep(40);
	}
}

void Sprite::SetSpriteType(BOOL spriteType)
{
	this->type = spriteType;
}

void Sprite::SetWindowSize(int height, int width)
{
	this->windowHeight = height;
	this->windowWidth = width;
}
